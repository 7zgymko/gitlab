# This file is a template, and might need editing before it works on your project.
FROM centos:7

RUN yum -update -y && yum install -y python3 python3-pip
RUN pip3 install flask flask_restful flask_jsonpify

MKDIR /opt/python_api
ADD python-api.py /opt/python_api/python-api.py

ENTRYPOINT ["python3,"/opt/python_api/python-api.py"]
